var games = [
    {
        "name": "League of Legends",
        "id": 1,
        "class": 'imgs',
        "image": 'img/GameBox.jpg',
        "pcIDS": [
            111,
            222
        ]
    },
    {
        "name": "Fortnite",
        "id": 2,
        "class": 'imgs',
        "image": 'img/fortnite.jpg',
        "pcIDS": [
            333,
            444
        ]
    },
    {
        "name": "Counter Strike GO",
        "id": 3,
        "class": 'imgs',
        "image": 'img/cs-go.jpg',
        "pcIDS": [
            555,
            666
        ]
    },
    {
        "name": "Overwatch",
        "id": 4,
        "class": 'imgs',
        "image": 'img/overwatch.jpg',
        "pcIDS": [
            777,
            888
        ]
    },
    {
        "name": "World of Warcraft",
        "id": 5,
        "class": 'imgs',
        "image": 'img/wow.jpg',
        "pcIDS": [
            999,
            1111
        ]
    },
    {
        "name": "Tomb Raider",
        "id": 6,
        "class": 'imgs',
        "image": 'img/tomb-raider2.jpg',
        "pcIDS": [
            2222,
            3333
        ]
    },
    {
        "name": "Witcher 3",
        "id": 7,
        "class": 'imgs',
        "image": 'img/witcher3.jpg',
        "pcIDS": [
            4444,
            5555
        ]
    },
    {
        "name": "Assassin's Creed",
        "id": 8,
        "class": 'imgs',
        "image": 'img/asassins-creed.jpg',
        "pcIDS": [
            6666,
            7777
        ]
    },
    {
        "name": "Assassin's Creed",
        "id": 9,
        "class": 'imgs',
        "image": 'img/apex-legends2.jpg',
        "pcIDS": [
            8888,
            9999
        ]
    },
    {
        "name": "FIFA 2019",
        "id": 10,
        "class": 'imgs',
        "image": 'img/fifa-2019.jpg',
        "pcIDS": [
            11111,
            22222
        ]
    },
    {
        "name": "Call of Duty",
        "id": 11,
        "class": 'imgs',
        "image": 'img/call-of-duty.jpg',
        "pcIDS": [
            44444,
            55555
        ]
    },
    {
        "name": "Far Cry 5",
        "id": 12,
        "class": 'imgs',
        "image": 'img/far-cry5.jpg',
        "pcIDS": [
            66666,
            77777
        ]
    }
];

var html = '<div class="row product-cust-containers">';
for (s=0; s<games.length; s++) {
var game = games[s];
html += "<div class='col-lg-2 col-md-2 col-sm-4 col-xs-4 mobile-padding hiddens'>";
html += "<h4 class='text-center hidden-xs hidden-sm'>"+game.name+"</h4>";
html += "<img src="+game.image+" id="+game.id+"  class='"+game.class+" img-responsive center-block'  data-pc='"+game.pcIDS+"'/>";
html += "</div>";
}
$("#games").append(html);


$(document).ready(function(){
    $(".imgs").click(function(){

     var id = $(this).attr('id');
     var pcs = $(this).data('pc').split(",");
     console.log(pcs);
     $(".hiddens").addClass("hidden")
    });
  });
